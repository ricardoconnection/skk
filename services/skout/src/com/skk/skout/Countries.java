/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/

package com.skk.skout;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Countries generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`countries`", schema = "public")
public class Countries implements Serializable {

    private Integer id;
    private String name;
    private String abbreviation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`name`", nullable = true, length = 255)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "`abbreviation`", nullable = true, length = 2)
    public String getAbbreviation() {
        return this.abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Countries)) return false;
        final Countries countries = (Countries) o;
        return Objects.equals(getId(), countries.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

