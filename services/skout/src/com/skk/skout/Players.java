/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/

package com.skk.skout;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Players generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`players`", schema = "public")
public class Players implements Serializable {

    private Integer id;
    private String name;
    private String surname;
    private Date birthdate;
    private String birthcountry;
    private Integer height;
    private Integer weight;
    private Character bestFoot;
    private Users users;
    private List<PlayerAgencies> playerAgencieses = new ArrayList<>();
    private List<PlayerTeams> playerTeamses = new ArrayList<>();
    private List<GamesPlayed> gamesPlayeds = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`name`", nullable = true, length = 255)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "`surname`", nullable = true, length = 255)
    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "`birthdate`", nullable = true)
    public Date getBirthdate() {
        return this.birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @Column(name = "`birthcountry`", nullable = true, length = 2)
    public String getBirthcountry() {
        return this.birthcountry;
    }

    public void setBirthcountry(String birthcountry) {
        this.birthcountry = birthcountry;
    }

    @Column(name = "`height`", nullable = true, scale = 0, precision = 10)
    public Integer getHeight() {
        return this.height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Column(name = "`weight`", nullable = true, scale = 0, precision = 10)
    public Integer getWeight() {
        return this.weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Column(name = "`bestFoot`", nullable = true, length = 1)
    public Character getBestFoot() {
        return this.bestFoot;
    }

    public void setBestFoot(Character bestFoot) {
        this.bestFoot = bestFoot;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`user_id`", referencedColumnName = "`id`", insertable = true, updatable = true)
    public Users getUsers() {
        return this.users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "players")
    public List<PlayerAgencies> getPlayerAgencieses() {
        return this.playerAgencieses;
    }

    public void setPlayerAgencieses(List<PlayerAgencies> playerAgencieses) {
        this.playerAgencieses = playerAgencieses;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "players")
    public List<PlayerTeams> getPlayerTeamses() {
        return this.playerTeamses;
    }

    public void setPlayerTeamses(List<PlayerTeams> playerTeamses) {
        this.playerTeamses = playerTeamses;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "players")
    public List<GamesPlayed> getGamesPlayeds() {
        return this.gamesPlayeds;
    }

    public void setGamesPlayeds(List<GamesPlayed> gamesPlayeds) {
        this.gamesPlayeds = gamesPlayeds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Players)) return false;
        final Players players = (Players) o;
        return Objects.equals(getId(), players.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

