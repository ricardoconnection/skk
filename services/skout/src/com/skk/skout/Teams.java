/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/

package com.skk.skout;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Teams generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`teams`", schema = "public")
public class Teams implements Serializable {

    private Integer id;
    private String name;
    private List<PlayerTeams> playerTeamses = new ArrayList<>();
    private List<CompetitionTeams> competitionTeamses = new ArrayList<>();
    private List<Games> gamesesForTeam2 = new ArrayList<>();
    private List<Games> gamesesForTeam1 = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`name`", nullable = true, length = 255)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "teams")
    public List<PlayerTeams> getPlayerTeamses() {
        return this.playerTeamses;
    }

    public void setPlayerTeamses(List<PlayerTeams> playerTeamses) {
        this.playerTeamses = playerTeamses;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "teams")
    public List<CompetitionTeams> getCompetitionTeamses() {
        return this.competitionTeamses;
    }

    public void setCompetitionTeamses(List<CompetitionTeams> competitionTeamses) {
        this.competitionTeamses = competitionTeamses;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "teamsByTeam2")
    public List<Games> getGamesesForTeam2() {
        return this.gamesesForTeam2;
    }

    public void setGamesesForTeam2(List<Games> gamesesForTeam2) {
        this.gamesesForTeam2 = gamesesForTeam2;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "teamsByTeam1")
    public List<Games> getGamesesForTeam1() {
        return this.gamesesForTeam1;
    }

    public void setGamesesForTeam1(List<Games> gamesesForTeam1) {
        this.gamesesForTeam1 = gamesesForTeam1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teams)) return false;
        final Teams teams = (Teams) o;
        return Objects.equals(getId(), teams.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

