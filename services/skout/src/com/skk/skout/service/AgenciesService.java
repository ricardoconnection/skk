/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/

package com.skk.skout.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.skk.skout.Agencies;

/**
 * Service object for domain model class Agencies.
 *
 * @see {@link Agencies}
 */
public interface AgenciesService {

    /**
     * Creates a new Agencies.
     *
     * @param agencies The information of the created CompositeTable.
     * @return The created Agencies.
     */
	Agencies create(Agencies agencies);


	/**
	 * Finds Agencies by id.
	 *
	 * @param agenciesId The id of the wanted Agencies.
	 * @return The found Agencies. If no Agencies is found, this method returns null.
	 */
	Agencies getById(Integer agenciesId) throws EntityNotFoundException;

	/**
	 * Updates the information of a Agencies.
	 *
	 * @param agencies The information of the updated Agencies.
	 * @return The updated Agencies.
     *
	 * @throws EntityNotFoundException if no Agencies is found with given id.
	 */
	Agencies update(Agencies agencies) throws EntityNotFoundException;

    /**
	 * Deletes a Agencies.
	 *
	 * @param agenciesId The id of the deleted Agencies.
	 * @return The deleted Agencies.
     *
	 * @throws EntityNotFoundException if no Agencies is found with the given id.
	 */
	Agencies delete(Integer agenciesId) throws EntityNotFoundException;

	/**
	 * Finds all Agencies.
	 *
	 * @return A list of Agencies.
	 */
    @Deprecated
	Page<Agencies> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Finds all Agencies.
	 * @return A list of Agencies.
	 */
    Page<Agencies> findAll(String query, Pageable pageable);

    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Agencies in the repository with matching query.
     *
     * @param query query to filter results.
	 * @return The count of the Agencies.
	 */
	long count(String query);

    Page<Agencies> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);

}

