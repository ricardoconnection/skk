/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/

package com.skk.skout.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.skk.skout.Players;

/**
 * Service object for domain model class Players.
 *
 * @see {@link Players}
 */
public interface PlayersService {

    /**
     * Creates a new Players.
     *
     * @param players The information of the created CompositeTable.
     * @return The created Players.
     */
	Players create(Players players);


	/**
	 * Finds Players by id.
	 *
	 * @param playersId The id of the wanted Players.
	 * @return The found Players. If no Players is found, this method returns null.
	 */
	Players getById(Integer playersId) throws EntityNotFoundException;

	/**
	 * Updates the information of a Players.
	 *
	 * @param players The information of the updated Players.
	 * @return The updated Players.
     *
	 * @throws EntityNotFoundException if no Players is found with given id.
	 */
	Players update(Players players) throws EntityNotFoundException;

    /**
	 * Deletes a Players.
	 *
	 * @param playersId The id of the deleted Players.
	 * @return The deleted Players.
     *
	 * @throws EntityNotFoundException if no Players is found with the given id.
	 */
	Players delete(Integer playersId) throws EntityNotFoundException;

	/**
	 * Finds all Players.
	 *
	 * @return A list of Players.
	 */
    @Deprecated
	Page<Players> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Finds all Players.
	 * @return A list of Players.
	 */
    Page<Players> findAll(String query, Pageable pageable);

    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Players in the repository with matching query.
     *
     * @param query query to filter results.
	 * @return The count of the Players.
	 */
	long count(String query);

    Page<Players> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);

}

