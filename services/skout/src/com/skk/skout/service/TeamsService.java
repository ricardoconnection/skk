/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/

package com.skk.skout.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.skk.skout.Teams;

/**
 * Service object for domain model class Teams.
 *
 * @see {@link Teams}
 */
public interface TeamsService {

    /**
     * Creates a new Teams.
     *
     * @param teams The information of the created CompositeTable.
     * @return The created Teams.
     */
	Teams create(Teams teams);


	/**
	 * Finds Teams by id.
	 *
	 * @param teamsId The id of the wanted Teams.
	 * @return The found Teams. If no Teams is found, this method returns null.
	 */
	Teams getById(Integer teamsId) throws EntityNotFoundException;

	/**
	 * Updates the information of a Teams.
	 *
	 * @param teams The information of the updated Teams.
	 * @return The updated Teams.
     *
	 * @throws EntityNotFoundException if no Teams is found with given id.
	 */
	Teams update(Teams teams) throws EntityNotFoundException;

    /**
	 * Deletes a Teams.
	 *
	 * @param teamsId The id of the deleted Teams.
	 * @return The deleted Teams.
     *
	 * @throws EntityNotFoundException if no Teams is found with the given id.
	 */
	Teams delete(Integer teamsId) throws EntityNotFoundException;

	/**
	 * Finds all Teams.
	 *
	 * @return A list of Teams.
	 */
    @Deprecated
	Page<Teams> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Finds all Teams.
	 * @return A list of Teams.
	 */
    Page<Teams> findAll(String query, Pageable pageable);

    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Teams in the repository with matching query.
     *
     * @param query query to filter results.
	 * @return The count of the Teams.
	 */
	long count(String query);

    Page<Teams> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);

}

