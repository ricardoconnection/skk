/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/

package com.skk.skout;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * UserRoles generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`user_roles`", schema = "public")
public class UserRoles implements Serializable {

    private Integer id;
    private Roles roles;
    private Users users;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`role_id`", referencedColumnName = "`id`", insertable = true, updatable = true)
    public Roles getRoles() {
        return this.roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`user_id`", referencedColumnName = "`id`", insertable = true, updatable = true)
    public Users getUsers() {
        return this.users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRoles)) return false;
        final UserRoles userRoles = (UserRoles) o;
        return Objects.equals(getId(), userRoles.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

