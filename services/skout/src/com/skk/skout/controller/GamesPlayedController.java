/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/
package com.skk.skout.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.skk.skout.GamesPlayed;
import com.skk.skout.service.GamesPlayedService;
import com.wordnik.swagger.annotations.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class GamesPlayed.
 * @see GamesPlayed
 */
@RestController("skout.GamesPlayedController")
@RequestMapping("/skout/GamesPlayed")
@Api(description = "Exposes APIs to work with GamesPlayed resource.", value = "GamesPlayedController")
public class GamesPlayedController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GamesPlayedController.class);

    @Autowired
    @Qualifier("skout.GamesPlayedService")
    private GamesPlayedService gamesPlayedService;

    /**
     * @deprecated Use {@link #findGamesPlayeds(String)} instead.
     */
    @Deprecated
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of GamesPlayed instances matching the search criteria.")
    public Page<GamesPlayed> findGamesPlayeds(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering GamesPlayeds list");
        return gamesPlayedService.findAll(queryFilters, pageable);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of GamesPlayed instances matching the search criteria.")
    public Page<GamesPlayed> findGamesPlayeds(@RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering GamesPlayeds list");
        return gamesPlayedService.findAll(query, pageable);
    }

    @RequestMapping(value = "/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @ApiOperation(value = "Returns downloadable file for the data.")
    public Downloadable exportGamesPlayeds(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        return gamesPlayedService.export(exportType, query, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service GamesPlayedService instance
	 */
    protected void setGamesPlayedService(GamesPlayedService service) {
        this.gamesPlayedService = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Creates a new GamesPlayed instance.")
    public GamesPlayed createGamesPlayed(@RequestBody GamesPlayed gamesplayed) {
        LOGGER.debug("Create GamesPlayed with information: {}", gamesplayed);
        gamesplayed = gamesPlayedService.create(gamesplayed);
        LOGGER.debug("Created GamesPlayed with information: {}", gamesplayed);
        return gamesplayed;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of GamesPlayed instances.")
    public Long countGamesPlayeds(@RequestParam(value = "q", required = false) String query) {
        LOGGER.debug("counting GamesPlayeds");
        return gamesPlayedService.count(query);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the GamesPlayed instance associated with the given id.")
    public GamesPlayed getGamesPlayed(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting GamesPlayed with id: {}", id);
        GamesPlayed foundGamesPlayed = gamesPlayedService.getById(id);
        LOGGER.debug("GamesPlayed details with id: {}", foundGamesPlayed);
        return foundGamesPlayed;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Updates the GamesPlayed instance associated with the given id.")
    public GamesPlayed editGamesPlayed(@PathVariable(value = "id") Integer id, @RequestBody GamesPlayed gamesplayed) throws EntityNotFoundException {
        LOGGER.debug("Editing GamesPlayed with id: {}", gamesplayed.getId());
        gamesplayed.setId(id);
        gamesplayed = gamesPlayedService.update(gamesplayed);
        LOGGER.debug("GamesPlayed details with id: {}", gamesplayed);
        return gamesplayed;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Deletes the GamesPlayed instance associated with the given id.")
    public boolean deleteGamesPlayed(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting GamesPlayed with id: {}", id);
        GamesPlayed deletedGamesPlayed = gamesPlayedService.delete(id);
        return deletedGamesPlayed != null;
    }
}
