/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/
package com.skk.skout.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.skk.skout.CompetitionTeams;
import com.skk.skout.Competitions;
import com.skk.skout.Games;
import com.skk.skout.service.CompetitionTeamsService;
import com.skk.skout.service.CompetitionsService;
import com.skk.skout.service.GamesService;
import com.wordnik.swagger.annotations.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class Competitions.
 * @see Competitions
 */
@RestController("skout.CompetitionsController")
@RequestMapping("/skout/Competitions")
@Api(description = "Exposes APIs to work with Competitions resource.", value = "CompetitionsController")
public class CompetitionsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompetitionsController.class);

    @Autowired
    @Qualifier("skout.CompetitionsService")
    private CompetitionsService competitionsService;

    @Autowired
    @Qualifier("skout.CompetitionTeamsService")
    private CompetitionTeamsService competitionTeamsService;

    @Autowired
    @Qualifier("skout.GamesService")
    private GamesService gamesService;

    /**
     * @deprecated Use {@link #findCompetitions(String)} instead.
     */
    @Deprecated
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of Competitions instances matching the search criteria.")
    public Page<Competitions> findCompetitions(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Competitions list");
        return competitionsService.findAll(queryFilters, pageable);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of Competitions instances matching the search criteria.")
    public Page<Competitions> findCompetitions(@RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Competitions list");
        return competitionsService.findAll(query, pageable);
    }

    @RequestMapping(value = "/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @ApiOperation(value = "Returns downloadable file for the data.")
    public Downloadable exportCompetitions(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        return competitionsService.export(exportType, query, pageable);
    }

    @RequestMapping(value = "/{id:.+}/competitionTeamses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the competitionTeamses instance associated with the given id.")
    public Page<CompetitionTeams> findAssociatedCompetitionTeamses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated competitionTeamses");
        return competitionTeamsService.findAssociatedValues(id, "CompetitionTeams", "id", pageable);
    }

    @RequestMapping(value = "/{id:.+}/gameses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the gameses instance associated with the given id.")
    public Page<Games> findAssociatedGameses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated gameses");
        return gamesService.findAssociatedValues(id, "Games", "id", pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service CompetitionsService instance
	 */
    protected void setCompetitionsService(CompetitionsService service) {
        this.competitionsService = service;
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service CompetitionTeamsService instance
	 */
    protected void setCompetitionTeamsService(CompetitionTeamsService service) {
        this.competitionTeamsService = service;
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service GamesService instance
	 */
    protected void setGamesService(GamesService service) {
        this.gamesService = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Creates a new Competitions instance.")
    public Competitions createCompetitions(@RequestBody Competitions competitions) {
        LOGGER.debug("Create Competitions with information: {}", competitions);
        competitions = competitionsService.create(competitions);
        LOGGER.debug("Created Competitions with information: {}", competitions);
        return competitions;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of Competitions instances.")
    public Long countCompetitions(@RequestParam(value = "q", required = false) String query) {
        LOGGER.debug("counting Competitions");
        return competitionsService.count(query);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the Competitions instance associated with the given id.")
    public Competitions getCompetitions(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Competitions with id: {}", id);
        Competitions foundCompetitions = competitionsService.getById(id);
        LOGGER.debug("Competitions details with id: {}", foundCompetitions);
        return foundCompetitions;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Updates the Competitions instance associated with the given id.")
    public Competitions editCompetitions(@PathVariable(value = "id") Integer id, @RequestBody Competitions competitions) throws EntityNotFoundException {
        LOGGER.debug("Editing Competitions with id: {}", competitions.getId());
        competitions.setId(id);
        competitions = competitionsService.update(competitions);
        LOGGER.debug("Competitions details with id: {}", competitions);
        return competitions;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Deletes the Competitions instance associated with the given id.")
    public boolean deleteCompetitions(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Competitions with id: {}", id);
        Competitions deletedCompetitions = competitionsService.delete(id);
        return deletedCompetitions != null;
    }
}
