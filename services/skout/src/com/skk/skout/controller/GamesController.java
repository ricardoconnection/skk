/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/
package com.skk.skout.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.skk.skout.Games;
import com.skk.skout.GamesPlayed;
import com.skk.skout.service.GamesPlayedService;
import com.skk.skout.service.GamesService;
import com.wordnik.swagger.annotations.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class Games.
 * @see Games
 */
@RestController("skout.GamesController")
@RequestMapping("/skout/Games")
@Api(description = "Exposes APIs to work with Games resource.", value = "GamesController")
public class GamesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GamesController.class);

    @Autowired
    @Qualifier("skout.GamesService")
    private GamesService gamesService;

    @Autowired
    @Qualifier("skout.GamesPlayedService")
    private GamesPlayedService gamesPlayedService;

    /**
     * @deprecated Use {@link #findGames(String)} instead.
     */
    @Deprecated
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of Games instances matching the search criteria.")
    public Page<Games> findGames(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Games list");
        return gamesService.findAll(queryFilters, pageable);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of Games instances matching the search criteria.")
    public Page<Games> findGames(@RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Games list");
        return gamesService.findAll(query, pageable);
    }

    @RequestMapping(value = "/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @ApiOperation(value = "Returns downloadable file for the data.")
    public Downloadable exportGames(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        return gamesService.export(exportType, query, pageable);
    }

    @RequestMapping(value = "/{id:.+}/gamesPlayeds", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the gamesPlayeds instance associated with the given id.")
    public Page<GamesPlayed> findAssociatedGamesPlayeds(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated gamesPlayeds");
        return gamesPlayedService.findAssociatedValues(id, "GamesPlayed", "id", pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service GamesService instance
	 */
    protected void setGamesService(GamesService service) {
        this.gamesService = service;
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service GamesPlayedService instance
	 */
    protected void setGamesPlayedService(GamesPlayedService service) {
        this.gamesPlayedService = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Creates a new Games instance.")
    public Games createGames(@RequestBody Games games) {
        LOGGER.debug("Create Games with information: {}", games);
        games = gamesService.create(games);
        LOGGER.debug("Created Games with information: {}", games);
        return games;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of Games instances.")
    public Long countGames(@RequestParam(value = "q", required = false) String query) {
        LOGGER.debug("counting Games");
        return gamesService.count(query);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the Games instance associated with the given id.")
    public Games getGames(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Games with id: {}", id);
        Games foundGames = gamesService.getById(id);
        LOGGER.debug("Games details with id: {}", foundGames);
        return foundGames;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Updates the Games instance associated with the given id.")
    public Games editGames(@PathVariable(value = "id") Integer id, @RequestBody Games games) throws EntityNotFoundException {
        LOGGER.debug("Editing Games with id: {}", games.getId());
        games.setId(id);
        games = gamesService.update(games);
        LOGGER.debug("Games details with id: {}", games);
        return games;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Deletes the Games instance associated with the given id.")
    public boolean deleteGames(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Games with id: {}", id);
        Games deletedGames = gamesService.delete(id);
        return deletedGames != null;
    }
}
