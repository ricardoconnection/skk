/*Copyright (c) 2016-2017 connectionltda.com.br All Rights Reserved.
 This software is the confidential and proprietary information of connectionltda.com.br You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with connectionltda.com.br*/
package com.skk.skout.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.skk.skout.Countries;
import com.skk.skout.service.CountriesService;
import com.wordnik.swagger.annotations.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class Countries.
 * @see Countries
 */
@RestController("skout.CountriesController")
@RequestMapping("/skout/Countries")
@Api(description = "Exposes APIs to work with Countries resource.", value = "CountriesController")
public class CountriesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountriesController.class);

    @Autowired
    @Qualifier("skout.CountriesService")
    private CountriesService countriesService;

    /**
     * @deprecated Use {@link #findCountries(String)} instead.
     */
    @Deprecated
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of Countries instances matching the search criteria.")
    public Page<Countries> findCountries(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Countries list");
        return countriesService.findAll(queryFilters, pageable);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of Countries instances matching the search criteria.")
    public Page<Countries> findCountries(@RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Countries list");
        return countriesService.findAll(query, pageable);
    }

    @RequestMapping(value = "/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @ApiOperation(value = "Returns downloadable file for the data.")
    public Downloadable exportCountries(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        return countriesService.export(exportType, query, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service CountriesService instance
	 */
    protected void setCountriesService(CountriesService service) {
        this.countriesService = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Creates a new Countries instance.")
    public Countries createCountries(@RequestBody Countries countries) {
        LOGGER.debug("Create Countries with information: {}", countries);
        countries = countriesService.create(countries);
        LOGGER.debug("Created Countries with information: {}", countries);
        return countries;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of Countries instances.")
    public Long countCountries(@RequestParam(value = "q", required = false) String query) {
        LOGGER.debug("counting Countries");
        return countriesService.count(query);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the Countries instance associated with the given id.")
    public Countries getCountries(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Countries with id: {}", id);
        Countries foundCountries = countriesService.getById(id);
        LOGGER.debug("Countries details with id: {}", foundCountries);
        return foundCountries;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Updates the Countries instance associated with the given id.")
    public Countries editCountries(@PathVariable(value = "id") Integer id, @RequestBody Countries countries) throws EntityNotFoundException {
        LOGGER.debug("Editing Countries with id: {}", countries.getId());
        countries.setId(id);
        countries = countriesService.update(countries);
        LOGGER.debug("Countries details with id: {}", countries);
        return countries;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Deletes the Countries instance associated with the given id.")
    public boolean deleteCountries(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Countries with id: {}", id);
        Countries deletedCountries = countriesService.delete(id);
        return deletedCountries != null;
    }
}
